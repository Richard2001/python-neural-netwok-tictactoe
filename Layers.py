import numpy as np


class Layer:
    def __init__(self):
        self.input = None
        self.output = None

    def forward(self, x):
        pass

    def backward(self, output_gradient, learning_rate):
        pass


class Dense(Layer):
    def __init__(self, input_size, output_size):
        super().__init__()
        self.input = None
        self.output = None
        self.weights = np.random.randn(output_size, input_size)
        self.bias = np.random.randn(output_size, 1)

    def forward(self, x):
        self.input = x
        self.output = np.dot(self.weights, self.input) + self.bias
        return self.output

    def backward(self, output_gradient, learning_rate):
        weights_gradient = np.dot(output_gradient, self.input.T)
        input_gradient = np.dot(self.weights.T, output_gradient)
        self.weights -= learning_rate * weights_gradient
        self.bias -= learning_rate * output_gradient
        return input_gradient


class Activation(Layer):
    def __init__(self, activation, activation_prime):
        super().__init__()
        self.activation = activation
        self.activation_prime = activation_prime

    def forward(self, x):
        self.input = x
        self.output = self.activation()
        return self.output

    def backward(self, output_gradient, learning_rate):
        return np.multiply(output_gradient, self.activation_prime())


class Tanh(Activation):
    def tanh(self):
        return np.tanh(self.input)

    def tanh_prime(self):
        return 1 - self.output ** 2

    def __init__(self):
        super().__init__(self.tanh, self.tanh_prime)


class Sigmoid(Activation):
    def sigmoid(self):
        return 1 / (1 + np.exp(-self.input))

    def sigmoid_prime(self):
        return self.output * (1 - self.output)

    def __init__(self):
        super().__init__(self.sigmoid, self.sigmoid_prime)


class Softmax(Layer):
    def forward(self, x):
        tmp = np.exp(x)
        self.output = tmp / np.sum(tmp)
        return self.output

    def backward(self, output_gradient, learning_rate):
        n = np.size(self.output)
        return np.dot((np.identity(n) - self.output.T) * self.output, output_gradient)
