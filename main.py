import TicTacToe
from NeuralNetwork import *
from Layers import *
from Losses import *


nw = [
    Dense(18, 27),
    Sigmoid(),
    Dense(27, 27),
    Sigmoid(),
    Dense(27, 9),
    Softmax()
]

game = TicTacToe.Base()

x, y = TicTacToe.Base.generate_full_dataset(game)
network = NeuralNetwork(BinaryCrossEntropy(), nw)
network.train(x, y, error_less=0.0001, epochs=5000, learning_rate=0.5)

while True:
    print("New game")
    game.game_p1_nn(network)
    game.reset_game()
    i = input("New game? (y/n or 1/0)\t")
    while i != "y" and i != "n" and i != "1" and i != "0":
        print("Incorrect input!")
        i = input("New game? (y/n or 1/0)\t")
    if i == "n" or i == "0":
        break
