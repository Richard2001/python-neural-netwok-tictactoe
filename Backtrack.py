import numpy as np


class Node:
    blank = "?"
    p = "?"
    p_other = "?"
    status_checker = None

    def __init__(self, board, current_player, parent=None, move=None):
        self.board = board.copy()
        self.current_player = current_player
        self.parent = parent
        if parent is None:
            self.depth = 0
        else:
            self.depth = parent.depth + 1
        self.move = move
        self.heuristics = 0
        self.operator_i = 0
        operators = []
        for i in range(len(board)):
            if board[i] == self.blank:
                operators += [i]
        self.operators = operators
        for op in operators:
            new_board = board.copy()
            new_board[op] = self.p
            if self.status_checker(new_board) == self.p:
                self.operators = [op]
                return
        """
        if board.count(self.p_other) == 1:
            index = board.index(self.p_other)
            print(index)
            if index == 0 or index == 2 or index == 6 or index == 8:
                self.operators = [4]
                return
            if index == 4:
                self.operators = [0, 2, 6, 8]
        """
        emergency = 0
        operator = self.operators
        for op in operators:
            new_board = board.copy()
            new_board[op] = self.p_other
            if self.status_checker(new_board) == self.p_other:
                operator = [op]
                emergency += 1
        self.operators = operator
        if emergency > 1:
            self.change_heuristics(-2)



    def change_heuristics(self, value): # önmagának és a szülő csomópontjának is beállítja
        self.heuristics += value
        if self.parent is not None:
            self.parent.change_heuristics(value)

    def __eq__(self, other):
        if other is not Node:
            return False
        for i in range(len(self.board)):
            if self.board[i] != other.board[i]:
                return False
        return True


class Backtrack:
    def __init__(self):
        self.best_draw = None
        self.best_win = None
        self.p = None
        self.p_other = None
        self.next_boards = []

    def predict_next_move(self, game, board=None):
        Node.blank = game.blank
        Node.p = game.current_player
        Node.p_other = game.other_player()
        Node.status_checker = game.get_status
        self.p = game.current_player
        self.p_other = game.other_player()
        current_board = board
        if current_board is None:
            current_board = game.board
        node = Node(current_board, self.p)
        self.next_boards = []
        while node is not None:
            # megnézi a következő lépéshez tartozó csomópontot (tartalmazza a lépést és a heurisztikát)
            if node.depth == 1 and not self.next_boards.__contains__(node):
                self.next_boards.append(node)
            status = game.get_status(node.board)
            # megnézi az kiválasztott csomópont állapotát, ha valamelyik győzött az alapján heurisztikát módosít
            # és folytatja a keresést tovább
            if status != game.blank or node.operator_i >= len(node.operators):
                if status == self.p:
                    node.change_heuristics(1)
                elif status == game.draw:
                    node.change_heuristics(0)
                elif status == self.p_other:
                    node.change_heuristics(-1)
                node = node.parent
            else:
                op = node.operators[node.operator_i]
                node.operator_i += 1
                new_board = game.move(op, node.board, node.current_player)
                other_player = game.other_player(node.current_player)
                node = Node(new_board, other_player, node, op)

        best = self.next_boards[0]
        for i in range(len(self.next_boards)):
            # print(self.next_boards[i].move, self.next_boards[i].heuristics)
            if self.next_boards[i].heuristics > best.heuristics:
                best = self.next_boards[i]
        return best.move

    @staticmethod
    def get_dataset(move):
        ds = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        ds[move] = 1.0
        return np.array(ds).reshape(9, 1)
