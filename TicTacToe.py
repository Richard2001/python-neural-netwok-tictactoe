import Backtrack
import numpy as np


class Base:
    blank = "•"
    player1 = "X"
    player2 = "O"
    draw = player1 + player2
    current_player = player1

    def __init__(self):
        self.board = None
        self.reset_game()

    def reset_game(self):
        self.board = [
            self.blank,
            self.blank,
            self.blank,
            self.blank,
            self.blank,
            self.blank,
            self.blank,
            self.blank,
            self.blank
        ]
        self.current_player = self.player1

    def move(self, place, board=None, current_player=None):
        if board is None:
            board = self.board.copy()
        else:
            board = board.copy()
        if current_player is None:
            current_player = self.current_player
        board[place] = current_player
        return board

    def other_player(self, current_player=None):
        if current_player is None:
            current_player = self.current_player
        if current_player == self.player1:
            return self.player2
        return self.player1

    def can_move(self, place, board=None):
        if board is None:
            board = self.board
        if place < 0 or place > 8:
            return False
        if self.board[place] != self.blank:
            return False
        return True

    def get_status(self, board=None):
        if board is None:
            board = self.board
        for i in range(3):
            b = board
            row = i * 3
            # sorok
            if b[row] != self.blank and b[row] == b[row + 1] and b[row + 1] == b[row + 2]:
                return b[row]
            # oszlopok
            if b[i] != self.blank and b[i] == b[i + 3] and b[i + 3] == b[i + 6]:
                return b[i]
        if b[0] != self.blank and b[0] == b[4] and b[4] == b[8]:
            return b[0]
        elif b[6] != self.blank and b[6] == b[4] and b[4] == b[2]:
            return b[6]
        if not board.__contains__(self.blank):
            return self.draw
        return self.blank

    def select_next_move(self):
        place = None
        while True:
            place = input(f"{self.current_player}'s move: ")
            try:
                place = int(place) - 1
                if not self.can_move(place):
                    raise Exception()
                break
            except:
                print("Illegal move")
                print(self)
        return place

    def game_p1_p2(self):
        while self.get_status() == self.blank:
            self.board = self.move(self.select_next_move())
            print(self.get_board_string(self.board))
            self.current_player = self.other_player()
        print(self)
        print(f"Winner: {self.get_status()}")
        print(self)

    def game_p1_bt(self):
        bt = Backtrack.Backtrack()
        print(self)
        while self.get_status() == self.blank:
            self.board = self.move(self.select_next_move())
            print(self)
            self.current_player = self.other_player()
            if self.get_status() == self.blank:
                move = bt.predict_next_move(self)
                self.board = self.move(move)
                self.current_player = self.other_player()
                print(f"{self.player2}'s move: {move + 1}")
                print(self)
        print(f"Winner: {self.get_status()}")
        print(self)

    def game_p1_nn(self, network):
        print(self)
        while self.get_status() == self.blank:
            self.board = self.move(self.select_next_move())
            print(self)
            self.current_player = self.other_player()
            if self.get_status() == self.blank:
                ds = self.make_dataset(self.board, self.player1, self.player2)
                out = network.predict(ds)
                move = np.argmax(out)
                while not self.can_move(move, self.board):
                    out[move] = [-1]
                    print("Illegal move (AI)")
                    move = np.argmax(out)
                self.board = self.move(move)
                self.current_player = self.other_player()
                print(f"{self.player2}'s move: {move + 1}")
                print(self)
        print(f"Winner: {self.get_status()}")
        print(self)

    @staticmethod
    def make_dataset(board, player1, player2):
        p1 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        p2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in range(len(board)):
            if board[i] == player1:
                p1[i] = 1.0
            elif board[i] == player2:
                p2[i] = 1.0
        return np.array(p1 + p2).reshape(18,1)

    @staticmethod
    def get_board_string(board):
        s = ""
        line = ""
        for i in range(len(board)):
            line = line + board[i]
            if i % 3 == 2:
                s = line + "\n" + s
                line = ""
            else:
                line = line + " "
        return s

    def generate_full_dataset(self):
        bt = Backtrack.Backtrack()
        self.current_player = self.other_player()
        p1 = self.player1
        p2 = self.player2
        bl = self.blank
        x = []
        y = []
        current_boards = [self.board.copy()]
        for _ in range(4):
            new_boards = []
            for board in current_boards:
                for i in range(9):
                    if board[i] == bl:
                        b = board.copy()
                        b[i] = p1
                        dx = Base.make_dataset(b, p1, p2)
                        x += [dx]
                        move = bt.predict_next_move(self, b)
                        b[move] = p2
                        dy = Backtrack.Backtrack.get_dataset(move)
                        y += [dy]
                        if self.get_status(b) == bl:
                            new_boards += [b]
                        #print(self.get_board_string(b))

            current_boards = new_boards
        self.current_player = self.other_player()
        return np.array(x), np.array(y)

    def __str__(self):
        return Base.get_board_string(self.board)
