import numpy as np


class NeuralNetwork:
    def __init__(self, loss, network):
        self.loss = loss
        self.network = network

    def predict(self, x):
        output = x
        for layer in self.network:
            output = layer.forward(output)
        return output

    def train(self, x_train, y_train, error_less=0.0, epochs=10000, learning_rate=0.5, verbose=True):
        error = 0
        for e in range(epochs):
            error = 0
            for x, y in zip(x_train, y_train):
                output = self.predict(x)
                error += self.loss.loss(y, output)
                grad = self.loss.loss_prime(y, output)
                for layer in reversed(self.network):
                    grad = layer.backward(grad, learning_rate)
            error /= len(x_train)
            if error < error_less:
                break
            if verbose:
                print(f"{e + 1}/{epochs} | {error = }")
        print("Final error:", error)
